package com.example.challengetopfour.utils

import android.content.Context
import android.widget.Toast

class Constants {

    companion object{
        val PREF_IS_LOGIN = "PREF_IS_LOGIN"
        val PREF_USERNAME = "PREF_USERNAME"
        val PREF_EMAIL = "PREF_EMAIL"
        val PREF_PASSWORD = "PREF_PASSWORD"
        fun showMessage(text: String, context: Context) : Unit = Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
        val PREFS_NAME = "MY_PREF_LOGIN_REGISTER"
    }

}