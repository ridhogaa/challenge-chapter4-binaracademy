package com.example.challengetopfour.data.database.dao

import androidx.room.*
import com.example.challengetopfour.data.database.entity.NoteEntity

@Dao
interface NoteDAO {

    @Insert
    fun insertNote(note: NoteEntity)

    @Query("SELECT * FROM notes ORDER BY id_notes DESC ")
    fun getAllNotes() : List<NoteEntity>

    @Delete
    fun deleteNote(note: NoteEntity)

    @Update
    fun updateNote(note: NoteEntity)

}