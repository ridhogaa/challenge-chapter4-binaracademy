package com.example.challengetopfour.ui.home

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.challengetopfour.data.database.AppDatabase
import com.example.challengetopfour.data.database.entity.NoteEntity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class HomeViewModel (app: Application) : AndroidViewModel(app) {
    private var allNotes : MutableLiveData<List<NoteEntity>?> = MutableLiveData()

    init {
        getAllNotes()
    }

    fun getAllNotesObserver() : MutableLiveData<List<NoteEntity>?> {
        return allNotes
    }

    fun getAllNotes(){
        GlobalScope.launch {
            val notesDAO = AppDatabase.getInstance((getApplication()))!!.notesDao()
            val listNotes = notesDAO.getAllNotes()
            allNotes.postValue(listNotes)
        }
    }

    fun insertNotes(notes: NoteEntity){
        GlobalScope.async {
            val notesDAO = AppDatabase.getInstance((getApplication()))!!.notesDao()
            notesDAO.insertNote(notes)
            getAllNotes()
        }
    }

    fun deleteNotes(notes : NoteEntity){
        GlobalScope.launch {
            val notesDAO = AppDatabase.getInstance((getApplication()))!!.notesDao()
            notesDAO.deleteNote(notes)
            getAllNotes()
        }
    }

    fun updateNotes(notes: NoteEntity){
        GlobalScope.async {
            val notesDAO = AppDatabase.getInstance((getApplication()))!!.notesDao()
            notesDAO.updateNote(notes)
            getAllNotes()
        }
    }
}