package com.example.challengetopfour.ui.delete

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.example.challengetopfour.data.database.entity.NoteEntity
import com.example.challengetopfour.databinding.FragmentDeleteDialogBinding
import com.example.challengetopfour.ui.home.HomeViewModel
import com.example.challengetopfour.utils.Constants

class DeleteDialogFragment(private val notesEntity: NoteEntity) : DialogFragment() {
    private var _binding: FragmentDeleteDialogBinding? = null
    private val binding get() = _binding!!
    private lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDeleteDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val width = (resources.displayMetrics.widthPixels * 0.85).toInt()
        val height = (resources.displayMetrics.heightPixels * 0.40).toInt()
        dialog!!.window?.setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT)
        homeViewModel = ViewModelProvider(requireActivity()).get(HomeViewModel::class.java)

        binding.btnDelete.setOnClickListener {
            homeViewModel.deleteNotes(notesEntity)
            Constants.showMessage("Notes Berhasil Dihapus!", requireContext())
            dismiss()
        }

        binding.btnCancel.setOnClickListener {
            dismiss()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}