package com.example.challengetopfour.ui.register

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.challengetopfour.R
import com.example.challengetopfour.databinding.FragmentRegisterBinding
import com.example.challengetopfour.ui.login.LoginFragment
import com.example.challengetopfour.utils.Constants
import com.example.challengetopfour.utils.PreferencesHelper


class RegisterFragment : Fragment() {

    lateinit var sharedPreferences: PreferencesHelper
    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        sharedPreferences = PreferencesHelper(requireContext())
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnRegister.setOnClickListener {
            if (binding.etEmailRegister.text!!.isNotEmpty() && binding.etUsernameRegister.text!!.isNotEmpty()
                && binding.etPasswordRegister.text!!.isNotEmpty() && binding.etKonfirmPasswordRegister.text!!.isNotEmpty()
            ) {
                if (binding.etPasswordRegister.text.toString() == binding.etKonfirmPasswordRegister.text.toString()) {
                    sharedPreferences.put(
                        Constants.PREF_EMAIL,
                        binding.etEmailRegister.text.toString()
                    )
                    sharedPreferences.put(
                        Constants.PREF_USERNAME,
                        binding.etUsernameRegister.text.toString()
                    )
                    sharedPreferences.put(
                        Constants.PREF_PASSWORD,
                        binding.etPasswordRegister.text.toString()
                    )
                    Constants.showMessage("Register Berhasil, Silakan Login!", requireContext())
                    it.findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
                } else {
                    Constants.showMessage("Password tidak matching!", requireContext())
                }
            } else {
                Constants.showMessage("Input masih kosong!", requireContext())
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onStart() {
        super.onStart()
        if (sharedPreferences.getBoolean(Constants.PREF_IS_LOGIN)) {
            findNavController().navigate(R.id.action_registerFragment_to_homeFragment)
        }
    }
}