package com.example.challengetopfour.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.challengetopfour.R
import com.example.challengetopfour.data.database.entity.NoteEntity
import com.example.challengetopfour.databinding.FragmentHomeBinding
import com.example.challengetopfour.ui.add.AddDialogFragment
import com.example.challengetopfour.ui.delete.DeleteDialogFragment
import com.example.challengetopfour.ui.edit.EditDialogFragment
import com.example.challengetopfour.utils.Constants
import com.example.challengetopfour.utils.PreferencesHelper

class HomeFragment : Fragment() {

    private lateinit var sharedPreferences: PreferencesHelper
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private lateinit var homeAdapter: HomeAdapter
    private lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        sharedPreferences = PreferencesHelper(requireContext())
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView()
        homeViewModel = ViewModelProvider(requireActivity()).get(HomeViewModel::class.java)
        homeViewModel.getAllNotesObserver().observe(viewLifecycleOwner) {
            if (it != null) {
                if (it.isEmpty()) {
                    binding.stateKosong.visibility = View.VISIBLE
                    binding.notesRecyclerView.visibility = View.GONE

                } else {
                    binding.stateKosong.visibility = View.GONE
                    binding.notesRecyclerView.visibility = View.VISIBLE
                    homeAdapter.setNotes(it as ArrayList<NoteEntity>)
                    homeAdapter.notifyDataSetChanged()
                }
            }
        }

        binding.fabAddData.setOnClickListener {
            val addDialog = AddDialogFragment()
            addDialog.show(parentFragmentManager, "addDialog")
        }

        binding.txtWelcomeUsername.text =
            "Welcome, ${sharedPreferences.getString(Constants.PREF_USERNAME)}"

        binding.txtLogout.setOnClickListener {
            sharedPreferences.clear()
            Constants.showMessage("Keluar", requireContext())
            it.findNavController().navigate(R.id.action_homeFragment_to_loginFragment)
        }
    }

    private fun recyclerView() {
        homeAdapter = HomeAdapter()
        binding.notesRecyclerView.adapter = homeAdapter
        binding.notesRecyclerView.layoutManager = LinearLayoutManager(requireContext())
        homeAdapter.onIvDeleteClick = {
            val deleteDialog = DeleteDialogFragment(it)
            deleteDialog.show(parentFragmentManager, "deleteDialog")
        }
        homeAdapter.onIvEditClick = {
            val editDialog = EditDialogFragment(it)
            editDialog.show(parentFragmentManager, "editDialog")
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}