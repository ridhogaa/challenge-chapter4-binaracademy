package com.example.challengetopfour.ui.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.challengetopfour.R
import com.example.challengetopfour.databinding.FragmentLoginBinding
import com.example.challengetopfour.utils.Constants
import com.example.challengetopfour.utils.PreferencesHelper


class LoginFragment : Fragment() {

    lateinit var sharedPreferences: PreferencesHelper
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        sharedPreferences = PreferencesHelper(requireContext())
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.textBpa.setOnClickListener {
            it.findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }
        binding.btnLogin.setOnClickListener {
            if (binding.etEmail.text!!.isNotEmpty() && binding.etPassword.text!!.isNotEmpty()) {
                if (sharedPreferences.getString(Constants.PREF_EMAIL) == binding.etEmail.text.toString() &&
                    sharedPreferences.getString(Constants.PREF_PASSWORD) == binding.etPassword.text.toString()
                ) {
                    sharedPreferences.put(Constants.PREF_IS_LOGIN, true)
                    Constants.showMessage("Login Berhasil!", requireContext())
                    it.findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
                } else {
                    Constants.showMessage(
                        "Data tidak valid! Silakan masukan email atau password yang benar!",
                        requireContext()
                    )
                }
            } else {
                Constants.showMessage("Input masih kosong!", requireContext())
            }
        }
    }

    override fun onStart() {
        super.onStart()
        if (sharedPreferences.getBoolean(Constants.PREF_IS_LOGIN)) {
            findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}