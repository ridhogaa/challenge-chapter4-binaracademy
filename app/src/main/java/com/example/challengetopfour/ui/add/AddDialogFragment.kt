package com.example.challengetopfour.ui.add

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.example.challengetopfour.R
import com.example.challengetopfour.data.database.entity.NoteEntity
import com.example.challengetopfour.databinding.FragmentAddDialogBinding
import com.example.challengetopfour.databinding.FragmentHomeBinding
import com.example.challengetopfour.ui.home.HomeViewModel
import com.example.challengetopfour.utils.Constants

class AddDialogFragment : DialogFragment() {
    private var _binding: FragmentAddDialogBinding? = null
    private val binding get() = _binding!!
    private lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAddDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val width = (resources.displayMetrics.widthPixels * 0.85).toInt()
        val height = (resources.displayMetrics.heightPixels * 0.40).toInt()
        dialog!!.window?.setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT)
        homeViewModel = ViewModelProvider(requireActivity()).get(HomeViewModel::class.java)
        binding.btnAdd.setOnClickListener {
            val addNote: NoteEntity = NoteEntity(
                0,
                binding.etAddTitle.text.toString(),
                binding.etAddDescription.text.toString()
            )
            homeViewModel.insertNotes(addNote)
            Constants.showMessage("Notes Berhasil Ditambahkan!", requireContext())
            dismiss()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}