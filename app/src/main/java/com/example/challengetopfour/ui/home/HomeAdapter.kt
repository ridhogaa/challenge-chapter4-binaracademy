package com.example.challengetopfour.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.challengetopfour.data.database.entity.NoteEntity
import com.example.challengetopfour.databinding.NotesListBinding

class HomeAdapter : RecyclerView.Adapter<HomeAdapter.ViewHolder>() {
    var onIvDeleteClick: ((NoteEntity) -> Unit)? = null
    var onIvEditClick : ((NoteEntity) -> Unit)? = null
    var listNotes = ArrayList<NoteEntity>()

    class ViewHolder(var binding : NotesListBinding) : RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = NotesListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val notesData = listNotes[position]
        holder.binding.notesData = notesData
        holder.binding.ivDelete.setOnClickListener{
            onIvDeleteClick?.invoke(notesData)
        }
        holder.binding.ivEdit.setOnClickListener {
            onIvEditClick?.invoke(notesData)
        }
    }

    override fun getItemCount(): Int = listNotes.size

    fun setNotes(notes: ArrayList<NoteEntity>) {
        this.listNotes = notes
    }
}